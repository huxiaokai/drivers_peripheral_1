# Copyright (c) 2021-2023 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

hdf_audio_path = "./.."
import("//build/ohos.gni")
import("$hdf_audio_path/audio.gni")

config("audio_interface_config") {
  visibility = [ ":*" ]

  cflags = []
  if (drivers_peripheral_audio_feature_mono_to_stereo) {
    cflags += [ "-DMONO_TO_STEREO" ]
  }

  ldflags = [ "-Wl" ]
}

if (defined(ohos_lite)) {
  ohos_shared_library("hdi_audio_interface_lib_capture") {
    sources = [
      "$hdf_audio_path/hal/hdi_passthrough/src/audio_common.c",
      "adm_adapter/src/audio_interface_lib_capture.c",
      "adm_adapter/src/audio_interface_lib_common.c",
    ]

    include_dirs = [
      "$hdf_audio_path/hal/hdi_passthrough/include",
      "adm_adapter/include",
      "interfaces/include",
      "$hdf_audio_path/interfaces/include",
      "//third_party/bounds_checking_function/include",
      "//base/hiviewdfx/hilog_lite/interfaces/native/kits/",
    ]

    deps = [ "//third_party/bounds_checking_function:libsec_shared" ]
    external_deps = [
      "hdf_core:libhdf_utils",
      "hilog_featured_lite:hilog_shared",
    ]
    public_configs = [ ":audio_interface_config" ]
    defines = [ "_GNU_SOURCE" ]
    subsystem_name = "hdf"
    part_name = "drivers_peripheral_audio"
  }

  ohos_shared_library("hdi_audio_interface_lib_render") {
    sources = [
      "$hdf_audio_path/hal/hdi_passthrough/src/audio_common.c",
      "adm_adapter/src/audio_interface_lib_common.c",
      "adm_adapter/src/audio_interface_lib_render.c",
    ]

    include_dirs = [
      "$hdf_audio_path/hal/hdi_passthrough/include",
      "adm_adapter/include",
      "interfaces/include",
      "$hdf_audio_path/interfaces/include",
      "//third_party/bounds_checking_function/include",
      "//base/hiviewdfx/hilog_lite/interfaces/native/kits/",
    ]

    deps = [ "//third_party/bounds_checking_function:libsec_shared" ]
    external_deps = [
      "hdf_core:libhdf_utils",
      "hilog_featured_lite:hilog_shared",
    ]
    defines = [ "_GNU_SOURCE" ]
    if (enable_audio_hal_hdf_log) {
      defines += [ "AUDIO_HDF_LOG" ]
    }
    public_configs = [ ":audio_interface_config" ]
    subsystem_name = "hdf"
    part_name = "drivers_peripheral_audio"
  }
} else {
  ohos_shared_library("hdi_audio_interface_lib_capture") {
    sources = [ "$hdf_audio_path/hal/hdi_passthrough/src/audio_common.c" ]
    if (drivers_peripheral_audio_feature_alsa_lib) {
      sources += [
        "//third_party/cJSON/cJSON.c",
        "alsa_adapter/src/alsa_lib_capture.c",
        "alsa_adapter/src/alsa_lib_common.c",
        "alsa_adapter/src/alsa_mixer_path.c",
      ]
    } else {
      sources += [
        "adm_adapter/src/audio_interface_lib_capture.c",
        "adm_adapter/src/audio_interface_lib_common.c",
      ]
    }
    include_dirs = [
      "$hdf_audio_path/hal/hdi_passthrough/include",
      "$hdf_audio_path/interfaces/include",
      "interfaces/include",
      "//third_party/bounds_checking_function/include",
    ]

    deps = []

    if (enable_audio_hal_hdf_log) {
      defines = [ "AUDIO_HDF_LOG" ]
    }

    if (drivers_peripheral_audio_feature_alsa_lib) {
      include_dirs += [
        "//third_party/alsa-lib/include",
        "alsa_adapter/include",
        "//third_party/cJSON",
      ]
      deps += [ "//third_party/alsa-lib:libasound" ]
    } else {
      include_dirs += [ "adm_adapter/include" ]
    }
    if (is_standard_system) {
      external_deps = [
        "c_utils:utils",
        "hdf_core:libhdf_utils",
        "hiviewdfx_hilog_native:libhilog",
      ]
    } else {
      external_deps = [ "hilog:libhilog" ]
    }

    public_configs = [ ":audio_interface_config" ]

    install_images = [ chipset_base_dir ]
    subsystem_name = "hdf"
    part_name = "drivers_peripheral_audio"
  }

  ohos_shared_library("hdi_audio_interface_lib_render") {
    sources = [ "$hdf_audio_path/hal/hdi_passthrough/src/audio_common.c" ]
    if (drivers_peripheral_audio_feature_alsa_lib) {
      sources += [
        "//third_party/cJSON/cJSON.c",
        "alsa_adapter/src/alsa_lib_common.c",
        "alsa_adapter/src/alsa_lib_render.c",
        "alsa_adapter/src/alsa_mixer_path.c",
      ]
    } else {
      sources += [
        "adm_adapter/src/audio_interface_lib_common.c",
        "adm_adapter/src/audio_interface_lib_render.c",
      ]
    }

    include_dirs = [
      "$hdf_audio_path/hal/hdi_passthrough/include",
      "$hdf_audio_path/interfaces/include",
      "interfaces/include",
      "//third_party/bounds_checking_function/include",
    ]

    deps = []

    if (enable_audio_hal_hdf_log) {
      defines = [ "AUDIO_HDF_LOG" ]
    }

    if (drivers_peripheral_audio_feature_alsa_lib) {
      include_dirs += [
        "alsa_adapter/include",
        "//third_party/alsa-lib/include",
        "//third_party/cJSON",
      ]
      deps += [ "//third_party/alsa-lib:libasound" ]
    } else {
      include_dirs += [ "adm_adapter/include" ]
    }
    if (is_standard_system) {
      external_deps = [
        "c_utils:utils",
        "hdf_core:libhdf_utils",
        "hiviewdfx_hilog_native:libhilog",
      ]
    } else {
      external_deps = [ "hilog:libhilog" ]
    }

    public_configs = [ ":audio_interface_config" ]

    install_images = [ chipset_base_dir ]
    subsystem_name = "hdf"
    part_name = "drivers_peripheral_audio"
  }
}
